package com.glearning.bankingapp.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.glearning.bankingapp.model.Customer;
import com.glearning.bankingapp.model.IndianBank;
import com.glearning.bankingapp.service.BankingService;

public class Client {

	private final static Scanner scanner = new Scanner(System.in);
	private static BankingService bankingService;
	private static List<Customer> customers = new ArrayList<>();

	public static void main(String[] args) {
		Customer ramesh = initializeCustomer();
		boolean isCredentialsValid = customerLogin(ramesh);
		if (isCredentialsValid) {
			System.out.println("Credentials entered are valid ");
			displayOptions();
			execute(ramesh);
		} else {
			System.out.println("Credentials entered are invalid. Please try again!! ");
		}
	}

	private static void execute(Customer customer) {
		System.out.println();
		System.out.println("Choose the option from above:: ");
		int option = scanner.nextInt();
		switch (option) {
		case 1:
			checkAccountBalance(customer);
			break;
		case 2:
			deposit(customer);
			break;
		case 3:
			withdraw(customer);
			break;
		case 4:
			transfer(customer);
			break;		
		case 5:
			logout(customer);
			break;		
		}
	}

	private static void logout(Customer customer) {
		System.out.println("Thank you for using the application.");
		System.exit(1);
		
	}

	private static void transfer(Customer customer) {
		System.out.println("Enter the customer name to be transferred the amount to :: ");
		String customerAccountNumber = scanner.next();
		
		//find the customer from the list
		for(Customer c: customers) {
			if( c.getAccountNumber().equals(customerAccountNumber)) {
				long otp = generateOTP();
				System.out.println("Please enter the OTP generated:: ");
				long otpEntered = scanner.nextInt();
				
				if (otpEntered == otp) {
					System.out.println("OTP validated successfully :: ");
					System.out.println("Enter the amount to transfer :: ");
					int amount = scanner.nextInt();
					
					bankingService.transfer(amount, c);
					
				} else {
					System.out.println("invalid otp");
				}
				execute(customer);
			}
		}
		System.out.println("No Matching customers found: ");
		execute(customer);
	}

	private static long generateOTP() {
		long otp = (long) (Math.random() * 8000) + 2000;
		System.out.println("The OTP generated is : "+ otp);
		return otp; 
		
	}

	private static void withdraw(Customer customer) {
		System.out.println("Enter the amount to withdraw :: ");
		int amount = scanner.nextInt();
		double withdrawAmount = bankingService.withdraw(amount);
		System.out.println("The amount withdrawn is :: "+ withdrawAmount);
		System.out.println("The updated account balance is "+ customer.getBalance());
		execute(customer);
		
	}

	private static void deposit(Customer customer) {
		System.out.println("Enter the amount to deposit :: ");
		int amount = scanner.nextInt();
		bankingService.deposit(amount);
		System.out.println("The updated account balance is "+ customer.getBalance());
		execute(customer);

	}

	private static void checkAccountBalance(Customer customer) {
		double accountBalance = bankingService.checkBalance();
		System.out.println("The account balance is "+ accountBalance);
		execute(customer);
	}

	private static boolean customerLogin(Customer customer) {
		System.out.println("*********** Login ***************");
		System.out.println("Please enter your account number");
		String accountNo = scanner.next();

		System.out.println("Please enter your password");
		String password = scanner.next();
		if (accountNo.equals(customer.getAccountNumber()) && password.equals(customer.getPassword())) {
			return true;
		}
		return false;
	}

	private static void displayOptions() {
		System.out.println("***********************");
		System.out.println("Please enter your option");
		System.out.println(" 1 -> Check your account balance ");
		System.out.println(" 2 -> Deposit ");
		System.out.println(" 3 -> Withdraw ");
		System.out.println(" 4 -> Transfer");
		System.out.println(" 5 -> Logout");
		System.out.println("Choose your option:: ");
		System.out.println("***********************");
	}

	private static Customer initializeCustomer() {
		Customer ramesh = new Customer("080303453456", "Welcome@123", "9845125485");
		ramesh.setBalance(4000);
		
		Customer suresh = new Customer("1111", "Welcome@123", "9845125485");
		suresh.setBalance(10_000);
		customers.add(suresh);
		
		Customer vimal = new Customer("2222", "Welcome@123", "9845125485");
		vimal.setBalance(5000);
		customers.add(vimal);
		
		bankingService = new IndianBank(ramesh);
		return ramesh;
	}
}
