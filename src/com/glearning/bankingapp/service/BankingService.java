package com.glearning.bankingapp.service;

import com.glearning.bankingapp.model.Customer;

public interface BankingService {
	
	double checkBalance();
	
	double deposit(double amount);
	
	double withdraw(double amount);
	
	void transfer(double amount, Customer customer);
	

}
