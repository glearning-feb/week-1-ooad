package com.glearning.bankingapp.model;

public class Customer {
	
	//SA003444, 34340-2340234
	private String accountNumber;
	private String password;
	//+91-32434, 91-34324
	private String phoneNumber;
	
	private double balance;

	public Customer(String accountNumber, String password, String phoneNumber) {
		this.accountNumber = accountNumber;
		this.password = password;
		this.phoneNumber = phoneNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public double getBalance() {
		return balance;
	}
	
	public void setBalance(double amount) {
		this.balance = amount;
	}
	
}
