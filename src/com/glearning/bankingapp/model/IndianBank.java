package com.glearning.bankingapp.model;

import com.glearning.bankingapp.service.BankingService;

public class IndianBank implements BankingService {
	private final Customer customer;

	public IndianBank(Customer customer) {
		this.customer = customer;
	}

	@Override
	public double checkBalance() {
		return this.customer.getBalance();
	}

	@Override
	public double deposit(double amount) {
		double currentAccountBalance = this.customer.getBalance();
		double updatedAccountBalance = currentAccountBalance + amount;
		this.customer.setBalance(updatedAccountBalance);
		return updatedAccountBalance;
	}

	@Override
	public double withdraw(double amount) {
		double currentAccountBalance = this.customer.getBalance();
		if (currentAccountBalance - amount >= 0) {
			double updatedAccountBalance = currentAccountBalance - amount;
			this.customer.setBalance(updatedAccountBalance);
			return amount;
		}
		return 0;
	}

	@Override
	public void transfer(double amount, Customer customer) {
		double currentAccountBalance = this.customer.getBalance();
		if (currentAccountBalance - amount >= 0) {
			double updatedAccountBalance = currentAccountBalance - amount;
			this.customer.setBalance(updatedAccountBalance);
			customer.setBalance(amount + customer.getBalance());
		}
	}
}
